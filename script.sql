USE [EmployeeManagement]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 3/18/2021 9:15:44 PM ******/
DROP TABLE [dbo].[Employee]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/18/2021 9:15:44 PM ******/
DROP TABLE [dbo].[Account]
GO
USE [master]
GO
/****** Object:  Database [EmployeeManagement]    Script Date: 3/18/2021 9:15:44 PM ******/
DROP DATABASE [EmployeeManagement]
GO
/****** Object:  Database [EmployeeManagement]    Script Date: 3/18/2021 9:15:44 PM ******/
CREATE DATABASE [EmployeeManagement]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'EmployeeManagement', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\EmployeeManagement.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'EmployeeManagement_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\EmployeeManagement_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [EmployeeManagement] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [EmployeeManagement].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [EmployeeManagement] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [EmployeeManagement] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [EmployeeManagement] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [EmployeeManagement] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [EmployeeManagement] SET ARITHABORT OFF 
GO
ALTER DATABASE [EmployeeManagement] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [EmployeeManagement] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [EmployeeManagement] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [EmployeeManagement] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [EmployeeManagement] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [EmployeeManagement] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [EmployeeManagement] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [EmployeeManagement] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [EmployeeManagement] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [EmployeeManagement] SET  DISABLE_BROKER 
GO
ALTER DATABASE [EmployeeManagement] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [EmployeeManagement] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [EmployeeManagement] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [EmployeeManagement] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [EmployeeManagement] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [EmployeeManagement] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [EmployeeManagement] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [EmployeeManagement] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [EmployeeManagement] SET  MULTI_USER 
GO
ALTER DATABASE [EmployeeManagement] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [EmployeeManagement] SET DB_CHAINING OFF 
GO
ALTER DATABASE [EmployeeManagement] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [EmployeeManagement] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [EmployeeManagement] SET DELAYED_DURABILITY = DISABLED 
GO
USE [EmployeeManagement]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 3/18/2021 9:15:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[username] [varchar](10) NOT NULL,
	[fullName] [nvarchar](50) NOT NULL,
	[password] [varchar](50) NOT NULL,
	[roles] [bit] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 3/18/2021 9:15:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Employee](
	[EmpID] [nvarchar](10) NOT NULL,
	[Fullname] [nvarchar](30) NOT NULL,
	[Phone] [varchar](15) NOT NULL,
	[Email] [varchar](30) NOT NULL,
	[Address] [nvarchar](300) NOT NULL,
	[DateOfBirth] [varchar](10) NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[EmpID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'phucldh', N'Lê Đắc Hoàng Phúc ', N'123456', 1)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'phuchgt', N'Hoàng Gia Thiên Phúc ', N'123456', 1)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'bachnv', N'Nguyễn Việt Bách', N'123456', 0)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'huylng', N'Lê Nguyễn Gia Huy', N'123456', 0)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'baoddq', N'Dương Đình Quốc Bảo', N'123456', 0)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'huyvl', N'Vũ Lâm huy', N'123456', 0)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'linhvtt', N'Vũ Thị Thùy Linh', N'123456', 0)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'tramlnq', N'Lê Nguyên Quỳnh Trâm', N'123456', 0)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'trucntt', N'Nguyễn Thị Thanh Trúc', N'123456', 0)
INSERT [dbo].[Account] ([username], [fullName], [password], [roles]) VALUES (N'xoanntt', N'Nguyễn Thị Thanh Xoan', N'123456', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E001', N'Le Dac Hoang Phuc', N'1111111111', N'phucldh@gmail.com', N'Tan Phu', N'09/24/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E002', N'Hoang Gia Thien Phuc', N'2222222222', N'phuchgt@gmail.com', N'Buon Me Thuot', N'03/13/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E003', N'Le Nguyen Gia Huy', N'3333333333', N'huylng@gmail.com', N'Binh Duong', N'08/26/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E004', N'Nguyen Viet Bach', N'4444444444', N'bachnv@gmail.com', N'Binh Thanh', N'04/01/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E005', N'Duong Dinh Quoc Bao', N'5555555555', N'baoddq@gmail.com', N'Tan Binh', N'04/30/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E006', N'Vu Thi Thuy Linh', N'6666666666', N'linhvtt@gmail.com', N'Binh Phuoc', N'03/02/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E007', N'Nguyen Thi Thanh Truc', N'7777777777', N'trucntt@gmail.com', N'Quan 9', N'08/14/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E008', N'Vu Lam Huy', N'8888888888', N'huyvl@gmail.com', N'Binh Phuoc', N'01/03/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E009', N'Nguyen Si Trong', N'9999999999', N'trongns@gmail.com', N'Bien Hoa', N'08/08/2000', 0)
INSERT [dbo].[Employee] ([EmpID], [Fullname], [Phone], [Email], [Address], [DateOfBirth], [IsDelete]) VALUES (N'E010', N'Ta Minh Nhat', N'1234567890', N'nhattm@gmail.com', N'Tan Phu', N'03/02/2000', 0)
USE [master]
GO
ALTER DATABASE [EmployeeManagement] SET  READ_WRITE 
GO

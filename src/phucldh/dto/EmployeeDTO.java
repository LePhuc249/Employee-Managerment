/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.dto;

import java.util.Date;

/**
 *
 * @author Admin
 */
public class EmployeeDTO {
    private String EmpID;
    private String Fullname;
    private String Phone;
    private String Email;
    private String Address;
    private Date DateOfBirth;
    private boolean isDelete;

    public EmployeeDTO(boolean isDelete,String EmpID, String Fullname, String Phone, String Email, String Address, Date DateOfBirth) {
        this.isDelete = isDelete;
        this.EmpID = EmpID;
        this.Fullname = Fullname;
        this.Phone = Phone;
        this.Email = Email;
        this.Address = Address;
        this.DateOfBirth = DateOfBirth;
    }

    public String getEmpID() {
        return EmpID;
    }

    public void setEmpID(String EmpID) {
        this.EmpID = EmpID;
    }

    public String getFullname() {
        return Fullname;
    }

    public void setFullname(String Fullname) {
        this.Fullname = Fullname;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String Phone) {
        this.Phone = Phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String Address) {
        this.Address = Address;
    }

    public Date getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(Date DateOfBirth) {
        this.DateOfBirth = DateOfBirth;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public String toString() {
        return isDelete + ";" + EmpID + ";" + Fullname + ";" + Phone + ";" + Email + ";" + Address + ";" + convertTypeDate(DateOfBirth.toString());
    }
    
    public static String convertTypeDate(String dateNow) {
        String day = dateNow.substring(8, 10);
        String month = dateNow.substring(14, 16);
        String year = dateNow.substring(24, 28);
        return month + "/" + day + "/" + year;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.dao;

import Employee.utils.MyConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Admin
 */
public class AccountDAO {
    Connection conn;
    PreparedStatement prepareSta;
    ResultSet rs;

    public void closeConnection() throws Exception {
        if (rs != null) {
            conn.close();
        }
        if (prepareSta != null) {
            conn.close();
        }
        if (conn != null) {
            conn.close();
        }
    }
    
    public String checkLogin(String userID, String password) throws Exception{
        String fullName = null;
        String sql =  "Select fullName from Account Where username = ? AND password = ?";
        conn = MyConnection.makeConnection();
        prepareSta = conn.prepareStatement(sql);
        prepareSta.setString(1, userID);
        prepareSta.setString(2, password);
        ResultSet rs = prepareSta.executeQuery();
        if (rs.next()) {
            fullName = rs.getString("fullName");
        }
        closeConnection();
        return fullName;
    }
    
    public int getRoleAccount(String fullName) throws Exception{
        int roles = 0;
        String sql = "Select roles from Account Where fullName = ?";
        conn = MyConnection.makeConnection();
        prepareSta = conn.prepareStatement(sql);
        prepareSta.setString(1, fullName);
        ResultSet rs = prepareSta.executeQuery();
        if (rs.next()) {
            roles = rs.getInt("roles");
        }
        closeConnection();
        return roles;
    }
}

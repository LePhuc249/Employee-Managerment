/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package phucldh.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import phucldh.dto.EmployeeDTO;

/**
 *
 * @author Admin
 */
public class EmployeeDAO {

    public static boolean saveToFile(List<EmployeeDTO> employeeList, String fileName) {
        boolean check = false;
        File f = null;
        PrintWriter pw = null;
        try {
            f = new File(fileName);
            pw = new PrintWriter(f);
            for (EmployeeDTO emloyee : employeeList) {
                pw.print(emloyee.toString() + "\n");
            }
            check = true;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            try {
                if (pw != null) {
                    pw.close();
                }
            } catch (Exception e) {
                System.out.println(e);
            }
        }
        return check;
    }

    public static List<EmployeeDTO> loadData(String fileName) {
        List<EmployeeDTO> employeeList = null;
        FileReader f = null;
        BufferedReader bf = null;
        EmployeeDTO employee = null;
        try {
            f = new FileReader(fileName);
            bf = new BufferedReader(f);
            employeeList = new ArrayList();
            String line = null;
            boolean isDelete;
            while ((line = bf.readLine()) != null) {
                line = line.trim();
                if (line.length() > 0) {
                    StringTokenizer stk = new StringTokenizer(line, ";");
                    String delete = stk.nextToken();
                    if (delete.equalsIgnoreCase("true")) {
                        isDelete = true;
                        String empId = stk.nextToken();
                        String fullName = stk.nextToken();
                        String phone = stk.nextToken();
                        String email = stk.nextToken();
                        String address = stk.nextToken();
                        String date = stk.nextToken();
                        Date dayOfBirth = new SimpleDateFormat("mm/dd/yyyy").parse(date);
                    } else if (delete.equalsIgnoreCase("false")) {
                        isDelete = false;
                        String empId = stk.nextToken();
                        String fullName = stk.nextToken();
                        String phone = stk.nextToken();
                        String email = stk.nextToken();
                        String address = stk.nextToken();
                        String date = stk.nextToken();
                        Date dayOfBirth = new SimpleDateFormat("mm/dd/yyyy").parse(date);
                        employee = new EmployeeDTO(isDelete, empId, fullName, phone, email, address, dayOfBirth);
                        employeeList.add(employee);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (bf != null) {
                    bf.close();
                }
                if (f != null) {
                    f.close();
                }
            } catch (Exception e) {
            }
        }
        return employeeList;
    }
}

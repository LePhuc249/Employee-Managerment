/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employee.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import phucldh.dao.EmployeeDAO;
import phucldh.dto.EmployeeDTO;

/**
 *
 * @author Admin
 */
public class EmployeeFunction {
    public final String filename = "employee.txt";
    public List<EmployeeDTO> listArmor = findAllArmor();
    
    public String convertTypeDate(String dateNow) {
        String day = dateNow.substring(8, 10);
        String month = dateNow.substring(14, 16);
        String year = dateNow.substring(24, 28);
        return month + "/" + day + "/" + year;
    }
    
    public List<EmployeeDTO> findAllArmor() {
        return EmployeeDAO.loadData(filename);
    }
    
    public boolean createArmor(EmployeeDTO dto) {
        listArmor.add(dto);
        return EmployeeDAO.saveToFile(listArmor, filename);
    }
    
    public EmployeeDTO findByArmorID(String id) {
        EmployeeDTO employee = null;
        List<EmployeeDTO> list = new ArrayList<>();
        list = findAllArmor();
        for (EmployeeDTO employeeDTO : list) {
            if (id.equalsIgnoreCase(employeeDTO.getEmpID())) {
                try {
                    String empId = employeeDTO.getEmpID();
                    String fullName = employeeDTO.getFullname();
                    String phone = employeeDTO.getPhone();
                    String email = employeeDTO.getEmail();
                    String address = employeeDTO.getAddress();
                    Date dayOfBirth = employeeDTO.getDateOfBirth();
                    boolean isDelete = false;
                    employee = new EmployeeDTO(isDelete, empId, fullName, phone, email, address, dayOfBirth);
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
        }
        return employee;
    }
    
    public boolean removeArmor(String id) {
        for (int i = 0; i < listArmor.size(); i++) {
            if (id.equalsIgnoreCase(listArmor.get(i).getEmpID().toString().trim())) {
                listArmor.get(i).setIsDelete(true);
            }
        }
        return EmployeeDAO.saveToFile(listArmor, filename);
    }
    
    public boolean updateArmor(EmployeeDTO dto) {
        for (int i = 0; i < listArmor.size(); i++) {
            if (dto.getEmpID().equalsIgnoreCase(listArmor.get(i).getEmpID().toString().trim())) {
                listArmor.remove(i);
                listArmor.add(dto);
            }
            Collections.sort(listArmor, new Comparator<EmployeeDTO>() {
                @Override
                public int compare(EmployeeDTO o1, EmployeeDTO o2) {
                    return o1.getEmpID().compareTo(o2.getEmpID());
                }
            });
        }
        return EmployeeDAO.saveToFile(listArmor, filename);
    }
}

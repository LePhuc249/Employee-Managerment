/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Employee.utils;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import phucldh.dto.EmployeeDTO;

/**
 *
 * @author Admin
 */
public class EmployeeDataTableModel<E> extends AbstractTableModel {
    
    String[] header;
    int[] indexes;
    ArrayList<EmployeeDTO> data;

    public EmployeeDataTableModel(String[] header, int[] indexes) {
        this.header = new String[header.length];
        for (int i = 0; i < header.length; i++) {
            this.header[i] = header[i];
        }
        this.indexes = new int[indexes.length];
        for (int i = 0; i < header.length; i++) {
            this.indexes[i] = indexes[i];
        }
        this.data = new ArrayList<EmployeeDTO>();
    }

    EmployeeDataTableModel() {

    }
    
    public ArrayList<EmployeeDTO> getData() {
        return data;
    }

    public void setData(ArrayList<EmployeeDTO> data) {
        this.data = data;
    }

    public void removeRow(int i) {
        data.remove(i);
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return header.length;
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (row < 0 || row > data.size() || column < 0 || column >= header.length) {
            return null;
        }
        EmployeeDTO emp = data.get(row);
        switch (indexes[column]) {
            case 0:
                return emp.getEmpID();
            case 1:
                return emp.getFullname();
            case 2:
                return emp.getPhone();
            case 3:
                return emp.getEmail();
        }
        return null;
    }

    @Override
    public String getColumnName(int column) {
        return (column >= 0 && column < header.length) ? header[column] : "";
    }
    
}
